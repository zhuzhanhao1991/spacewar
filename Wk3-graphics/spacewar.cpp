#include "spacewar.h"
#include "ship.h"

Spacewar::Spacewar()
{}

Spacewar::~Spacewar()
{
	releseALL();
}

void Spacewar::initialize( HWND hwnd )
{
	Game::initialize( hwnd );

	if ( !nebulaTexture.initialize( graphics, NEBULA_IMAGE ) ) // we can call a function in a "if statement" ? 
	{
		throw ( GameError( gameErrorNS::FATAL_ERROR, "Error initializing nebula texture" ) );
	}
	
	if ( !gameTextures.initialize( graphics, TEXTURE_IMAGE ) )
	{
		throw (GameError(gameErrorNS::FATAL_ERROR, "Error initializing games textures"));
	}

	if( !nebula.initialize( graphics, 0, 0, 0, &nebulaTexture ) )
	{
		throw ( GameError( gameErrorNS::FATAL_ERROR, "Error initializing nebula image" ) );
	}

	if (!planet.initialize(this, planetNS::WIDTH, planetNS::HEIGHT, planetNS::TEXTURE_COLS,&gameTextures))
	{
		throw ( GameError( gameErrorNS::FATAL_ERROR, "Error initializing planet" ) );
	}

	//place planet center screen
	planet.setX( GAME_WIDTH*0.5f - planet.getWidth()*0.5f );
	planet.setY( GAME_HEIGHT*0.5f - planet.getHeight()*0.5f );
		if( !ship1.initialize( this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &gameTextures) )
	{
		throw ( GameError( gameErrorNS::FATAL_ERROR, "Error initializing ship image" ) );
	}
	//starting screen position for the ship
		ship1.setX( GAME_WIDTH/4 );
		ship1.setY( GAME_HEIGHT/4 );

		ship1.setFrames( shipNS::SHIP1_START_FRAME, shipNS::SHIP1_END_FRAME );
		ship1.setCurrentFrame( shipNS::SHIP1_START_FRAME );
		ship1.setFrameDelay( shipNS::SHIP_ANIMATION_DELAY );

		ship1.setVelocity( VECTOR2( 0,-shipNS::SPEED ) );

		if (!ship2.initialize(this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &gameTextures))
		{
			throw (GameError(gameErrorNS::FATAL_ERROR, "Error initializing ship image"));
		}
		//starting screen position for the ship
		ship2.setX(GAME_WIDTH / 6);
		ship2.setY(GAME_HEIGHT / 6);

		ship2.setFrames(shipNS::SHIP2_START_FRAME, shipNS::SHIP2_END_FRAME);
		ship2.setCurrentFrame(shipNS::SHIP2_START_FRAME);
		ship2.setFrameDelay(shipNS::SHIP_ANIMATION_DELAY);

		ship2.setVelocity(VECTOR2(0, -shipNS::SPEED));
	return;
}

void Spacewar::update()
{
	ship1.gravityForce( &planet, frameTime );
	ship2.gravityForce(&planet, frameTime);
	
	ship1.update( frameTime );
	ship2.update(frameTime);
	planet.update( frameTime );

	if (input->isKeyDown(SHIP_UP_KEY))
	{

		// this set for Acceleration movement
		/*warrior1.setVelocity(VECTOR2(warrior1.getVelocity().x + frameTime * warriorNS::SPEED/100,
		warrior1.getVelocity().y + frameTime * warriorNS::SPEED/100
		));*/

		ship1.setX(ship1.getX() + cos((ship1.getDegrees()*PI) / 180)/* * warrior1.getVelocity().x*/);
		ship1.setY(ship1.getY() + sin((ship1.getDegrees()*PI) / 180) /** warrior1.getVelocity().y*/);

	}
	//------------------------------------------------------------------------------------------------------------------
	if (input->isKeyDown(SHIP_DOWN_KEY))
	{

		// this set for Acceleration movement
		/*warrior1.setVelocity(VECTOR2(warrior1.getVelocity().x - frameTime * warriorNS::SPEED / 100,
		warrior1.getVelocity().y - frameTime * warriorNS::SPEED / 100
		));*/

		ship1.setX(ship1.getX() + cos((ship1.getDegrees()*PI) / 180)/** warrior1.getVelocity().x*/);
		ship1.setY(ship1.getY() - sin((ship1.getDegrees()*PI) / 180)/** warrior1.getVelocity().y*/);

	}
	//==========================================================================================================
	// Right  rotation
	if (input->isKeyDown(SHIP_RIGHT_KEY))
		ship1.setDegrees(ship1.getDegrees() + frameTime *(180.0f / (float)PI));
	//==========================================================================================================
	// Left   rotation
	if (input->isKeyDown(SHIP_LEFT_KEY))
		ship1.setDegrees(ship1.getDegrees() - frameTime * (180.0f / (float)PI));
	//==========================================================================================================
}

void Spacewar::ai()
{}

void Spacewar::collisions()
{
	VECTOR2 collisionVector;
	if (ship1.collidesWith(planet, collisionVector))
	{
		//bounce off the planet
		ship1.bounce(collisionVector, planet);
		ship1.damage(PLANET);
	}
	if (ship2.collidesWith(planet, collisionVector))
	{
		//bounce off the planet
		ship2.bounce(collisionVector, planet);
		ship2.damage(PLANET);
	}
	if (ship1.collidesWith(ship2, collisionVector))
	{
		//bounce off the planet
		ship1.bounce(collisionVector, ship2);
		ship1.damage(SHIP);
		ship2.bounce(collisionVector * -1, ship1);
		ship2.damage(SHIP);

	}

}

void Spacewar::render()
{
	graphics->spriteBegin();
	// call draw functions here!

	nebula.draw();
	planet.draw();
	ship1.draw();
	ship2.draw();

	// call draw functions here!
	graphics->spriteEnd();
}

void Spacewar::releseALL()
{
	//shipTexture.onLostDevice();
	//planetTexture.onLostDevice();
	gameTextures.onLostDevice();
	nebulaTexture.onLostDevice();
	Game::releaseAll();

}

void Spacewar::resetALL()
{
	//planetTexture.onResetDevice();
	nebulaTexture.onResetDevice();
	//shipTexture.onResetDevice();
	gameTextures.onLostDevice();
	Game::resetAll();
}