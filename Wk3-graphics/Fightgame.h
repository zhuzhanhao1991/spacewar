#ifndef _FIGHTGAME_H
#define _FIGHTGAME_H
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "ship.h"
#include "warrior.h"
#include "planet.h"
#include <math.h>

class Fightgame : public Game
{
private:
	//variables
	//TextureManager nebulaTexture;
	//	TextureManager planetTexture;
	//	TextureManager shipTexture;
	//TextureManager gameTextures;
	TextureManager warriorTexture;
	TextureManager back_groundTexture;
	TextureManager planetTexture;
	TextureManager gameTextures;

	Image back_ground;
	Planet planet;
	//	Ship ship;
	Warrior warrior1;
	Warrior warrior2;
	Warrior warrior3;

public:
	Fightgame();
	virtual ~Fightgame();

	void initialize(HWND hwnd);
	void update();
	void ai();
	void collisions();
	void render();

	void releseALL();
	void resetALL();

};
#endif