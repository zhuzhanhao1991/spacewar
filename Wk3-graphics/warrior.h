#ifndef _WARRIOR_H_
#define _WARRIOR_H_
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace warriorNS
{

	const int WIDTH = 70;
	const int HEIGHT = 70;
	const int X = GAME_WIDTH / 2 - WIDTH / 2;
	const int Y = GAME_HEIGHT / 2 - HEIGHT / 2;

	const float SPEED = 100.0f;
	const float MASS = 300.0f;
	const float ROTATION_RATE = (float)PI / 4;

	const int TEXTURE_COLS = 15;

	const int WARRIOR2_START_FRAME = 3;
	const int WARRIOR2_END_FRAME = 3;

	const int WARRIOR1_START_FRAME = 1;	//starting frame
	const int WARRIOR1_END_FRAME = 1;	//ending frame
	const float WARRIOR_ANIMATION_DELAY = 0.9f;	// delay between frames

	//const int  SHIELD_START_FRAME = 24;
	//const int  SHIELD_END_FRAME = 27;
	//const float SHIELD_ANIMATION_DELAY = 0.3f;

}

class Warrior : public Entity
{
private:
	//bool shieldOn;
	//Image shield;
public:
	Warrior();

	void update(float frameTime);
	virtual void draw();
	virtual bool initialize(Game *gamePtr, int width, int height, int ncols, TextureManager *textureM);
	//void damage(WEAPON);
};

#endif
