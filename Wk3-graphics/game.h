// Game.h

#ifndef _GAME_H		//prevent multiple definitions of this class
#define _GAME_H

#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <MMSystem.h>
#include "graphics.h"
#include "constants.h"
#include "gameError.h"
#include "input.h"

class Game 
{
protected:
	// common game properties
	Graphics* graphics;			// pointer to Graphics
	Input* input;				// pointer to Input;
	HWND	hwnd;				// window handle
	HRESULT	hr;					// return type from Windows/DirectX
	LARGE_INTEGER timeStart;	// performance counter start value
	LARGE_INTEGER timeEnd;		// performance counter end value
	LARGE_INTEGER timerFreq;	// performance counter frequency
	float frameTime;			// time required for last frame ----- is a time(hours minutes and seconds)  or it a time(one time, tow times..)
	float fps;					// frames per second
	DWORD sleepTime;			// number of milliseconds to sleep between frames
	bool paused;				// true if game paused
	bool initialized;			// true if initialized

public:
	// constructor
	Game();
	// destructor
	virtual ~Game();

	// *** Member Functions ***
	
	// window message handler
	LRESULT messageHandler( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam );

	// initialize the game
	virtual void initialize( HWND hwnd ); 

	// call run repeatedly by the main message loop in WinMain
	virtual void run( HWND );

	// call when the graphics device is lost
	virtual void releaseAll();

	// recreate all surfaces and reset all entities
	virtual void resetAll();

	// delete all reserved memory
	virtual void deleteAll();

	// render game items
	virtual void renderGame();

	// handle lost graphics device
	virtual void handleLostGraphicsDevice();

	// return pointer to graphics
	Graphics* getGraphics() { return graphics; }

	// return pointer to input
	Input* getInput() { return input; }

	// exit the game
	void exitGame() { PostMessage( hwnd, WM_DESTROY, 0, 0 ); }

	// ** pure virtual function declarations **
	// update game items
	virtual void update() = 0;

	// perform AI calculations
	virtual void ai() = 0;
	
	// check for collisions
	virtual void collisions() = 0;

	// render graphics
	virtual void render() = 0;

};

#endif