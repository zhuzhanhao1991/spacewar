// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Image.cpp v1.0

#include "image.h"

//=============================================================================
// default constructor
//=============================================================================
Image::Image()
{
    initialized = false;            // set true when successfully initialized
    spriteData.width = 2;
    spriteData.height = 2;
    spriteData.x = 0.0;
    spriteData.y = 0.0;
    spriteData.scale = 1.0;
    spriteData.angle = 0.0;
    spriteData.rect.left = 0;       // used to select one frame from multi-frame image
    spriteData.rect.top = 0;
    spriteData.rect.right = spriteData.width;
    spriteData.rect.bottom = spriteData.height;
    spriteData.texture = NULL;      // the sprite texture (picture)
    spriteData.flipHorizontal = false;	
    spriteData.flipVertical = false;
    cols = 1;
    textureManager = NULL;
    startFrame = 0;
    endFrame = 0;
    currentFrame = 0;
    frameDelay = 1.0;               // default to 1 second per frame of animation
    animTimer = 0.0;
    visible = true;                 // the image is visible
    loop = true;                    // loop frames
    animComplete = false;
    graphics = NULL;                // link to graphics system
    colorFilter = graphicsNS::WHITE; // WHITE for no change
}

//=============================================================================
// destructor
//=============================================================================
Image::~Image()
{}

//=============================================================================
// Initialize the Image.
// Post: returns true if successful, false if failed
// pointer to Graphics
// width of Image in pixels  (0 = use full texture width)
// height of Image in pixels (0 = use full texture height)
// number of columns in texture (1 to n) (0 same as 1)
// pointer to TextureManager
//=============================================================================
/*
The Image��s initialize function receives a pointer to the Graphics object, the
width and height of the image in pixels, the number of columns in the texture, and a pointer
to a TextureManager that contains the texture.
*/
bool Image::initialize(Graphics *g, int width, int height, int ncols,
                       TextureManager *textureM)
{
    try{
        graphics = g;                               // the graphics object
        textureManager = textureM;                  // pointer to texture object

        spriteData.texture = textureManager->getTexture();
        if(width == 0) // use full width of texture
            width = textureManager->getWidth();    
        spriteData.width = width;
        if(height == 0)// use full height of texture
            height = textureManager->getHeight();   
        spriteData.height = height;
        cols = ncols;
        if (cols == 0)
            cols = 1;                               // if 0 cols use 1

        // configure spriteData.rect to draw currentFrame
        spriteData.rect.left = (currentFrame % cols) * spriteData.width;
        // right edge + 1
        spriteData.rect.right = spriteData.rect.left + spriteData.width;
        spriteData.rect.top = (currentFrame / cols) * spriteData.height;
        // bottom edge + 1
        spriteData.rect.bottom = spriteData.rect.top + spriteData.height;
    }
    catch(...) {return false;}
    initialized = true;                                // successfully initialized
    return true;
}


//=============================================================================
// Draw the image using color as filter
// The color parameter is optional, WHITE is assigned as default in image.h
// Pre : spriteBegin() is called
// Post: spriteEnd() is called
//=============================================================================
void Image::draw(COLOR_ARGB color)
{
    if (!visible || graphics == NULL)
        return;
    // get fresh texture incase onReset() was called
    spriteData.texture = textureManager->getTexture();
    if(color == graphicsNS::FILTER)                     // if draw with filter// filter defult with white.
        graphics->drawSprite(spriteData, colorFilter);  // use colorFilter
    else
        graphics->drawSprite(spriteData, color);        // use color as filter
}

//=============================================================================
// Draw this image using the specified SpriteData.
//   The current SpriteData.rect is used to select the texture.
// Pre : spriteBegin() is called
// Post: spriteEnd() is called
//=============================================================================
void Image::draw(SpriteData sd, COLOR_ARGB color)
{
    if (!visible || graphics == NULL)
        return;
    sd.rect = spriteData.rect;                  // use this Images rect to select texture
    sd.texture = textureManager->getTexture();  // get fresh texture incase onReset() was called

    if(color == graphicsNS::FILTER)             // if draw with filter
        graphics->drawSprite(sd, colorFilter);  // use colorFilter
    else
        graphics->drawSprite(sd, color);        // use color as filter
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
/*
The update function receives the current frameTime as a parameter. The update
function is where all code that needs to be synchronized with elapsed time should
be placed.
*/
void Image::update(float frameTime) //(135; 5.7 Simple Animation)
{
    if (endFrame - startFrame > 0)          // if animated sprite
    {
        // update keeps track of the total elapsed time by adding frameTime to animTimer
		// remember that our game runs only when there are no messages, so frameTime will vary
		animTimer += frameTime;             // total elapsed time  ??

		// if total elaped time is greater than frameDelay
        if (animTimer > frameDelay)
        {

            animTimer -= frameDelay;
			// the next frame is selected by incrememting the currentFrame variable
            currentFrame++;
			// if we've gone past the last frame
            if (currentFrame < startFrame || currentFrame > endFrame)
            {
				// and looping is true, restart at the beginning
                if(loop == true)            // if looping animation
                    currentFrame = startFrame;
                else                        // not looping animation
                {
					// otherwise we're done and lets stop.
                    currentFrame = endFrame;
                    animComplete = true;    // animation complete
                }
            }
            setRect();                      // set spriteData.rect  //  now the currentFrame++ ~> next loop setRect() function will 
																	// display the next texture.
			// this uses the current frame number to calculate the rectangular portion of the texture
			//    image to display. 
			// go to setRect() function
		}
    }
}

//=============================================================================
//  Set spriteData.rect to draw currentFrame
//=============================================================================
inline void Image::setRect() 
{
    // configure spriteData.rect to draw currentFrame
    spriteData.rect.left = (currentFrame % cols) * spriteData.width;
    // right edge + 1
    spriteData.rect.right = spriteData.rect.left + spriteData.width;
    spriteData.rect.top = (currentFrame / cols) * spriteData.height;
    // bottom edge + 1
    spriteData.rect.bottom = spriteData.rect.top + spriteData.height;       
} 
// go to setCurrentFrame()

//=============================================================================
// Set the current frame of the image in a multiple frame image
// use it to change to a different animation sequence
// or just to select a different texture to apply to the image from a multiple-texture image (damage?)
//=============================================================================
void Image::setCurrentFrame(int c) 
{
    if(c >= 0)
    {
        currentFrame = c;
        animComplete = false;
        setRect();                          // set spriteData.rect
    }
}



