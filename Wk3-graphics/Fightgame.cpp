#include <iostream>
#include "Fightgame.h"
#include <string>
#include <cmath>

Fightgame::Fightgame()
{}

Fightgame::~Fightgame()
{
	releseALL();
}

void Fightgame::initialize(HWND hwnd)
{
	Game::initialize(hwnd);
	//TextureManager
	//back_ground
	if (!back_groundTexture.initialize(graphics, BACK_GROUND_IMAGE)) // we can call a function in a "if statement" ? 
	{
		throw (GameError(gameErrorNS::FATAL_ERROR, "Error initializing nebula texture"));
	}
	//warrior
	if (!warriorTexture.initialize(graphics, WARRIOR_IMAGE))
	{
		throw (GameError(gameErrorNS::FATAL_ERROR, "Error initializing games textures"));
	}
	//planet
	if (!planetTexture.initialize(graphics, PLANET_IMAGE))
	{
		throw (GameError(gameErrorNS::FATAL_ERROR, "Error initializing planet"));
	}

	//Image: back_ground 
	if (!back_ground.initialize(graphics, 0, 0, 0, &back_groundTexture))
	{
		throw (GameError(gameErrorNS::FATAL_ERROR, "Error initializing nebula image"));
	}


	if (!planet.initialize(this, planetNS::WIDTH, planetNS::HEIGHT, planetNS::TEXTURE_COLS, &planetTexture))
	{
		throw (GameError(gameErrorNS::FATAL_ERROR, "Error initializing planet"));
	}
	planet.setX(GAME_WIDTH*0.5f - planet.getWidth()*0.5f);
	planet.setY(GAME_HEIGHT*0.5f - planet.getHeight()*0.5f);

	//======================================================================
	//Image: Warrior1
	if (!warrior1.initialize(this, warriorNS::WIDTH, warriorNS::HEIGHT, warriorNS::TEXTURE_COLS, &warriorTexture))
	{
		throw (GameError(gameErrorNS::FATAL_ERROR, "Error initializing ship image"));
	}
	//starting screen position for the ship
	warrior1.setFrames(warriorNS::WARRIOR1_START_FRAME, warriorNS::WARRIOR1_END_FRAME);
	warrior1.setCurrentFrame(warriorNS::WARRIOR1_START_FRAME);
	warrior1.setFrameDelay(warriorNS::WARRIOR_ANIMATION_DELAY);

	warrior1.setX(GAME_WIDTH / 4);
	warrior1.setY(GAME_HEIGHT / 4);
	//=======================================================================
	//Image: warrior 2 
	if (!warrior2.initialize(this, warriorNS::WIDTH, warriorNS::HEIGHT, warriorNS::TEXTURE_COLS, &warriorTexture))
	{
		throw (GameError(gameErrorNS::FATAL_ERROR, "Error initializing ship image"));
	}
	//starting screen position for the ship
	warrior2.setFrames(warriorNS::WARRIOR2_START_FRAME, warriorNS::WARRIOR2_END_FRAME);
	warrior2.setCurrentFrame(warriorNS::WARRIOR2_START_FRAME);
	warrior2.setFrameDelay(warriorNS::WARRIOR_ANIMATION_DELAY);
	
	warrior2.setX(100);
	warrior2.setY(200);
	//========================================================================

	return;
}
//============================================================================================================
//In Fightgame Engine normally we just put the control code in update function
//=============================================================================================================
void Fightgame::update()
{
	warrior1.update(frameTime);
	warrior2.update(frameTime);
	planet.update(frameTime);

	if (input->isKeyDown(WARRIOR_UP_KEY))
	{

		// this set for Acceleration movement
		/*warrior1.setVelocity(VECTOR2(warrior1.getVelocity().x + frameTime * warriorNS::SPEED/100, 
									 warrior1.getVelocity().y + frameTime * warriorNS::SPEED/100
										));*/

		warrior1.setX(warrior1.getX() + sin((warrior1.getDegrees()*PI) / 180) * warrior1.getVelocity().x);
		warrior1.setY(warrior1.getY() - cos((warrior1.getDegrees()*PI) / 180) * warrior1.getVelocity().y);
		
	}

	//------------------------------------------------------------------------------------------------------------------
	if (input->isKeyDown(WARRIOR_DOWN_KEY))
	{

		// this set for Acceleration movement
		/*warrior1.setVelocity(VECTOR2(warrior1.getVelocity().x - frameTime * warriorNS::SPEED/100,
									warrior1.getVelocity().y - frameTime * warriorNS::SPEED/100
									));*/

		warrior1.setX(warrior1.getX() - sin((warrior1.getDegrees()*PI) / 180)* warrior1.getVelocity().x);
		warrior1.setY(warrior1.getY() + cos((warrior1.getDegrees()*PI) / 180)* warrior1.getVelocity().y);

	}

	/*==========================================================================================================
	warrior1.setVelocity(argument);  because the argument is a VECTOR2, so we should use a VECTOR2 to pass the function.
		then we use the 
					   -----warrior1.setVelocity(VECTOR2(x,y));
		or we can set a VECTOR2 value first. then put the value into setVelocity function as our argument.
					v=VECTOR2(warrior1.getVelocity().x - (frameTime * warriorNS::SPEED / 10),
												 warrior1.getVelocity().y + (frameTime * warriorNS::SPEED / 10))
					   -----warrior1.setVelocity(v);
	
	=============================================================================================================*/

	//==========================================================================================================
	// Right  rotation
	if (input->isKeyDown(WARRIOR_RIGHT_KEY))
		warrior1.setDegrees(warrior1.getDegrees() + frameTime *(180.0f / (float)PI));
	//==========================================================================================================
	// Left   rotation
	if (input->isKeyDown(WARRIOR_LEFT_KEY))
		warrior1.setDegrees(warrior1.getDegrees() - frameTime * (180.0f / (float)PI));
	//==========================================================================================================
}

void Fightgame::ai()
{}

void Fightgame::collisions()
{
	VECTOR2 collisionVector;
	if (warrior1.collidesWith(planet, collisionVector))
	{
		//bounce off the planet
		warrior1.bounce(collisionVector, planet);
		/*ship1.damage(PLANET);*/
	}
	if (warrior2.collidesWith(planet, collisionVector))
	{
		//bounce off the planet
		warrior2.bounce(collisionVector, planet);
		/*ship2.damage(PLANET);*/
	}

	if (warrior1.collidesWith(warrior2, collisionVector))
	{
		//bounce off the planet
		warrior1.bounce(collisionVector, warrior2);
		/*warrior1.damage(SHIP);*/
		warrior2.bounce(collisionVector * -1, warrior1);
		/*ship2.damage(SHIP);*/
	}

}

void Fightgame::render()
{
	graphics->spriteBegin();
	// call draw functions here!

	back_ground.draw();
	warrior2.draw();
	planet.draw();
	warrior1.draw();

	// call draw functions here!
	graphics->spriteEnd();
}

void Fightgame::releseALL()
{
	//shipTexture.onLostDevice();
	//planetTexture.onLostDevice();
	warriorTexture.onLostDevice(); 
	gameTextures.onLostDevice();
	back_groundTexture.onLostDevice();
	Game::releaseAll();

}

void Fightgame::resetALL()
{
	//planetTexture.onResetDevice();
	warriorTexture.onResetDevice();
	//shipTexture.onResetDevice();
	back_groundTexture.onResetDevice();
	gameTextures.onResetDevice();
	Game::resetAll();
}