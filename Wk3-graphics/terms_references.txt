﻿DC:device context (26; 2.6)--- it is a structure
	In order to access a particular device, an application informs the GDI to load the ap
	propriate device driver and prepare the device for drawing operations. This is accomplished
	by creating a device context
	DC is a structure that defines a Graphics object and its properties. 

Direct3D
	is a graphics application programming interface (API) for Microsoft Windows.
	Direct3D is used to render three-dimensional graphics in applications where performance is important, such as games.

DirectX:
		is a collection of application programming interfaces (APIs) that provide low-level
		access to the Windows operating system.
		APIs:
			Direct3D:
				This is the primary graphics API. Contrary to what the name might imply, 
				Direct3D does support the creation of 2D graphics as well as 3D graphics.
			XAC:
				TThis API supports the loading and playing of multiple WAV files simultaneously 
				with control over most playback attributes. It is useful for sound effects and music.
			DirectInput:
				DirectInput is used to get input from legacy input devices such as joysticks, 
				racing wheels, etc. All new input devices will be accessed via XInput.
			XInput:
				This is a newer input API for Windows and Xbox 360. It works with Windows XP
				 Service Pack 1 and newer; it supports Xbox 360 controllers under Windows; 
				 and it does not work with legacy DirectInput devices.
			DirectPlay:
				DirectPlay is a network communications support. It allows writing
				games that connect with other players via the Internet or LANs.
			DirectSetup
				This API provides an easy way to support end-user installation of the
				DirectX runtime required to run your game.

DLL:
	A DLL (dynamic link library) is a Windows file that is used by program to call upon existing functions.
	 Essentially, they allow Windows and other programs to gain functionality without having to have
	 that functionality built-in.
	DLL files are an essential part of Windows programming, and lead to sleeker, more efficient programs.


GDI:
	The heart of "device independence" is the "graphics device interface" 



handle:
	is a Windows data type that is used to reference different objects. 

hInstance. 
	This is essentially a pointer to the application. It is used by some of the
	Windows functions to identify which application is calling them.

hPrevInstance. 
	Always NULL. It is an obsolete parameter that is only maintained
	in the parameter list for backwards compatibility with earlier versions of
	Windows.

HWND 
	is a "handle to a window" and is part of the Win32 API . 
	HWNDs are essentially pointers (IntPtr) with values that make them (sort of) 
	point to a window-structure data. In general HWNDs are part an example 
	for applying the ADT model.

lpCmdLine. 
	A pointer to a null-terminated string of command line parameters.
	Command line parameters are words or symbols that may be passed to an application
	when it is started.

LPCTSTR:
	L‌ong P‌ointer to a C‌onst T‌CHAR STR‌ing (Don't worry, a long pointer is the same as a pointer. 
	There were two flavors of pointers under 16-bit windows.) Here's the table: LPSTR = char* LPCSTR = const char*

LRESULT	
	Signed result of message processing.
	This type is declared in WinDef.h as follows:
	typedef LONG_PTR LRESULT;

MSG:
	Contains message information from a thread's message queue
	typedef struct tagMSG {
	  HWND   hwnd;
	  UINT   message;
	  WPARAM wParam;
	  LPARAM lParam;
	  DWORD  time;
	  POINT  pt;
	} MSG, *PMSG, *LPMSG;

mutex (34; 2.5 Using a Mutex to Prevent Multiple Instances)

nCmdShow. 
	How the window is to be shown.

Primitive:
	DirectX has several different graphic shapes that it can draw. These shapes are referred to as
	primitives. The process of drawing these graphic primitives is called rendering.

RECT:
	structure defines the coordinates of the upper-left and lower-right corners of a rectangle.
	typedef struct _RECT {
	  LONG left;
	  LONG top;
	  LONG right;
	  LONG bottom;
	} RECT, *PRECT;

rendering:
	DirectX has several different graphic shapes that it can draw. These shapes are referred to as
	primitives. The process of drawing these graphic primitives is called rendering.

sprite:
	a rectangal or circle that is used for hold on a texture.

WinUser.h file
	which is located in “C:Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Include”

wParam:
	contains the character code for the key just pressed. We check for nondisplayable
	  characters and tell Windows to play a beep sound just to acknowledge the keypress. 
	  If a displayable character is pressed, we read the value from wParam and save it in
	  the variable ch.

ZeroMemory:
	Fills a block of memory with zeros.
	To avoid any undesired effects of optimizing compilers, use the SecureZeroMemory function.
	void ZeroMemory(
					[in] PVOID  Destination,
					[in] SIZE_T Length
					);

(APIs)application programming interfaces应用编程接口.
device driver 设备驱动程序
(GDI)graphics device interface图形设备接口

