#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#define WIN32_LEAN_AND_MEAN 

#ifdef _DEBUG
#define D3D_DEBUG_INFO
#endif

#include <d3d9.h>
#include <d3dx9.h>
#include "constants.h"
#include "gameError.h"


// DirectX pointer types
#define LP_3DDEVICE LPDIRECT3DDEVICE9
#define LP_3D		LPDIRECT3D9
#define LP_SPRITE	LPD3DXSPRITE
#define LP_TEXTURE	LPDIRECT3DTEXTURE9
#define VECTOR2		D3DXVECTOR2

// color defines
#define COLOR_ARGB DWORD
#define SETCOLOR_ARGB(a,r,g,b) \
	((COLOR_ARGB)((((a)&0xff)<<24)|(((r)&0xff)<<16)|(((g)&0xff)<<8)|((b)&0xff)))

namespace graphicsNS
{
    // Some common colors
    // ARGB numbers range from 0 through 255
    // A = Alpha channel (transparency where 255 is opaque)
    // R = Red, G = Green, B = Blue
    const COLOR_ARGB ORANGE  = D3DCOLOR_ARGB(255,255,165,  0);
    const COLOR_ARGB BROWN   = D3DCOLOR_ARGB(255,139, 69, 19);
    const COLOR_ARGB LTGRAY  = D3DCOLOR_ARGB(255,192,192,192);
    const COLOR_ARGB GRAY    = D3DCOLOR_ARGB(255,128,128,128);
    const COLOR_ARGB OLIVE   = D3DCOLOR_ARGB(255,128,128,  0);
    const COLOR_ARGB PURPLE  = D3DCOLOR_ARGB(255,128,  0,128);
    const COLOR_ARGB MAROON  = D3DCOLOR_ARGB(255,128,  0,  0);
    const COLOR_ARGB TEAL    = D3DCOLOR_ARGB(255,  0,128,128);
    const COLOR_ARGB GREEN   = D3DCOLOR_ARGB(255,  0,128,  0);
    const COLOR_ARGB NAVY    = D3DCOLOR_ARGB(255,  0,  0,128);
    const COLOR_ARGB WHITE   = D3DCOLOR_ARGB(255,255,255,255);
    const COLOR_ARGB YELLOW  = D3DCOLOR_ARGB(255,255,255,  0);
    const COLOR_ARGB MAGENTA = D3DCOLOR_ARGB(255,255,  0,255);
    const COLOR_ARGB RED     = D3DCOLOR_ARGB(255,255,  0,  0);
    const COLOR_ARGB CYAN    = D3DCOLOR_ARGB(255,  0,255,255);
    const COLOR_ARGB LIME    = D3DCOLOR_ARGB(255,  0,255,  0);
    const COLOR_ARGB BLUE    = D3DCOLOR_ARGB(255,  0,  0,255);
    const COLOR_ARGB BLACK   = D3DCOLOR_ARGB(255,  0,  0,  0);
    const COLOR_ARGB FILTER  = D3DCOLOR_ARGB(  0,  0,  0,  0);  // use to specify drawing with colorFilter
    const COLOR_ARGB ALPHA25 = D3DCOLOR_ARGB( 64,255,255,255);  // AND with color to get 25% alpha
    const COLOR_ARGB ALPHA50 = D3DCOLOR_ARGB(128,255,255,255);  // AND with color to get 50% alpha //The alpha value determines how 
    const COLOR_ARGB BACK_COLOR = NAVY;                         // background color of game		   //transparent a color appears.

    enum DISPLAY_MODE{TOGGLE, FULLSCREEN, WINDOW};
}

//Each time we draw our sprite to the screen, we need to configure its properties. syntax(120).
struct SpriteData
{
	int			width;		// width of sprite in pixels
	int			height;		// height of sprite in pixels
	float		x;			// screen location (top left corner of sprite)
	float		y;			
	float		scale;		// scale of sprite (<1 smaller, >1 bigger)
	float		angle;		// rotation angle in radians!!
	RECT		rect;		// used to select an image from a larger texture 
	LP_TEXTURE	texture;	// pointer to the texture
	bool		flipHorizontal;	// if true, flip sprite horizontally (mirror)
	bool		flipVertical;	// if true, flip sprite vertically (upside-down)
};

class Graphics
{
private:
	// directX pointers and stuff
	LP_3D		direct3d; // interface
	LP_3DDEVICE device3d; 
	LP_SPRITE	sprite; 
	D3DPRESENT_PARAMETERS d3dpp; //structure.
	D3DDISPLAYMODE pMode;

	// other variables
	HRESULT		result;	
	HWND		hwnd;
	bool		fullscreen;
	int			width;
	int			height;
	COLOR_ARGB	backColor; // background color

	// intialize d3d presentation parameters
	void initD3Dpp();

public:
	Graphics();		//constructor
	virtual ~Graphics();	//deconstructor
	void releaseAll();	// releases direct3d and device3d

	// initialize directX graphics
	// throw GameError on error
	// hw = handle to window
	// width = width in pixels
	// height = height in pixels
	// fullscreen = true if fullscreen mode, false for Windowed
	void initialize( HWND hw, int width, int height, bool fullscreen );

	// Display the offscreen backbuffer to the screen
	HRESULT showBackBuffer();

	// test for lost device
	HRESULT getDeviceState();

	// reset the graphics device
	HRESULT reset();

	
	/*
	in rendering any DirectX primitive is to begin a DirectX scene. A scene is started by calling
	the DirectX BeginScene function. After all rendering is complete, the scene is ended by
	calling the DirectX EndScene function. The rendering of primitives outside a scene will fail.
	We need to add functions to our Graphics class in graphics.h to begin and end a scene.
	Our new functions are named beginScene and endScene. The beginScene function is
	also the perfect place to clear the back buffer, so we are relocating the device3d->Clear
	function call from the showBackbuffer function to beginScene
	*/
	// clear the backbuffer and beginScene
	HRESULT beginScene()
	{
		result = E_FAIL;
		if( device3d == NULL )
		{
			return result;
		}
		// clear backbuffer to backColor
		device3d->Clear( 0, NULL, D3DCLEAR_TARGET, backColor, 1.0F, 0 );
		result = device3d->BeginScene();
		return result;
	}

	// Call EndScene
	HRESULT endScene()
	{
		result = E_FAIL;
		if( device3d ) 
		{
			result = device3d->EndScene();
		}
		return result;
	}

	// set color used to clear screen
	void setBackColor( COLOR_ARGB c ) { backColor = c; }

	//check to see if our adapter is compatible with the screensize
	bool isAdapterCompatible();

	//get functions
	LP_3D	get3D()	{ return direct3d; } 
	LP_3DDEVICE get3Ddevice() { return device3d; }
	HDC getDC()	{ return GetDC(hwnd); }

	// load the texture into default D3D memory (normal texture use)
	HRESULT loadTexture( const char* filename, COLOR_ARGB transcolor, 
						 UINT &width, UINT &height, LP_TEXTURE &texture );

	// draw sprite described in the SpriteData structure
	void drawSprite( const SpriteData &spriteData, 
					  COLOR_ARGB color = graphicsNS::WHITE );


	// sprite begin
	void spriteBegin()
	{
		sprite->Begin( D3DXSPRITE_ALPHABLEND );
	}

	// sprite end
	void spriteEnd() 
	{
		sprite->End();
	}

	//vector encapsulation
	static float Vector2Length( const VECTOR2* v )
	{
		return D3DXVec2Length(v);
	}
	static float Vector2Dot( const VECTOR2* v1, const VECTOR2* v2 )
	{
		return D3DXVec2Dot( v1, v2 );
	}

	static void Vector2Normalize( VECTOR2* v )
	{
		D3DXVec2Normalize( v, v );
	}

	static VECTOR2* Vector2Transform( VECTOR2* v, D3DXMATRIX *m )
	{
		return D3DXVec2TransformCoord( v,v,m );
	}
};

#endif