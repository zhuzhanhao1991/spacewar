#ifndef _BULLET_H_
#define _BULLET_H_
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace BulletNS
{
	const int WIDTH = 70;
	const int HEIGHT = 70;
	const int X = GAME_WIDTH / 2 - WIDTH / 2;
	const int Y = GAME_HEIGHT / 2 - HEIGHT / 2;

	const float SPEED = 100.0f;
	const float MASS = 300.0f;
	const float ROTATION_RATE = (float)PI / 4;

	const int TEXTURE_COLS = 15;

	const int BULLET_START_FRAME = 0;
	const int BULLET_END_FRAME = 3;
	const float BULLET_ANIMATION_DELAY = 0.9f;	// delay between frames
}

class Bullet : public Entity
{
private:

public:
	Bullet();
	void update(float frameTime);
	virtual void draw();
	virtual bool initialize(Game *gamePtr, int width, int height, int ncols, TextureManager *textureM);

};
#endif