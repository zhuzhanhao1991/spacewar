Configuring a Project for DirectX (52; 3.8.1 Configuring a Project for DirectX)
	Enter ��$(DXSDK_DIR)\Include�� as shown in Figure 3.5 then click ��OK.�� This tells
	Visual Studio where to look for the DirectX include files.

Change the ��Runtime Library�� to ��Multi-threaded (/MT).�� This change will statically
link in the C Runtime Libraries, which allows our program to run on systems
that do not have the C Runtime Library DLL installed (MSVCP100.DLL or equivalent).