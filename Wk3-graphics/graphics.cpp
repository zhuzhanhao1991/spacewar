#include "graphics.h"

Graphics::Graphics()
{
	direct3d = NULL;
	device3d = NULL;
	fullscreen = false;
	width = GAME_WIDTH;
	height = GAME_HEIGHT;
	// set the background color
	backColor = SETCOLOR_ARGB(255,0,0,128); // dark blue
}

Graphics::~Graphics()
{
	releaseAll();
}
// in order to free memory 
void Graphics::releaseAll()
{
	SAFE_RELEASE(device3d);
	SAFE_RELEASE(direct3d);
}

void Graphics::initialize( HWND hw, int w, int h, bool full )
{
	hwnd = hw;
	width = w;
	height = h;
	fullscreen = full;

	// initialize Direct3D
	//the first step in writing a DirectX application is to 
	//create a Direct3D object. and get interface to it.---Direct3Dcreate9() function just do this.	 
	direct3d = Direct3DCreate9( D3D_SDK_VERSION ); //direct3d is a interface. 

	if( direct3d == NULL )
		throw( GameError( gameErrorNS::FATAL_ERROR,
							"Error Initializing Direct3d"));

	initD3Dpp(); // initialize D3D presentation parameters

	if( fullscreen ) // if fullscreen mode
	{
		if( isAdapterCompatible() ) // is the adapter compatible?
		{
			d3dpp.FullScreen_RefreshRateInHz = pMode.RefreshRate;
		}
		else {
			throw( GameError( gameErrorNS::FATAL_ERROR,
							  "The graphics device does not support the specified resolution" ));
		}
	}

	// determine if graphics card supports hardware texturing
	//   and lighting and vertex shaders
	D3DCAPS9 caps;
	DWORD behavior;
	result = direct3d->GetDeviceCaps( D3DADAPTER_DEFAULT, 
									  D3DDEVTYPE_HAL,
									  & caps );

	// if device doesn't support HW T&L or doesn't support 1.1 vertex
	// shaders in hardware, then switch to software vertex processing
	if( (caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) == 0 ||
			caps.VertexShaderVersion < D3DVS_VERSION(1,1) )
	{// use software only processing
		behavior = D3DCREATE_SOFTWARE_VERTEXPROCESSING; }
	else
	{		behavior = D3DCREATE_HARDWARE_VERTEXPROCESSING;}

	//------------------------------------------------------------------------------------------
	//the second step in writing a DirectX application is to call the CreateDevice() function.
	// this function returns a working device interface.
	// create Direct3d device
	/*---------------------------------------------------------------------------------
	HRESULT CreateDevice(                                        // references:(40; 3.3 Creating a Device)
						UINT Adapter,
						D3DDEVTYPE DeviceType,
						HWND hFocusWindow,
						DWORD BehaviorFlags,                  
						D3DPRESENT_PARAMETERS *pPresentationParameters, //structure that specifies properties for the device to be created. 
																		//We will use a separate function to fill this structure 
																		//(see our initD3Dpp() function below).
						IDirect3DDevice9 **ppReturnedDeviceInterface    //This is a return value. It contains the address
																		//of a pointer to the created device.
						);
	-------------------------------------------------------------------------------------*/
	result = direct3d->CreateDevice(
		D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		hwnd,
		behavior,
		&d3dpp,
		&device3d );

	if( FAILED( result ) )
		throw( GameError(gameErrorNS::FATAL_ERROR, 
							"Error creating direct3d device") );
//--------------------------------------------------------------------------------------------
// The sprite creation code
	result = D3DXCreateSprite( device3d, &sprite );
	if( FAILED( result ) )
		throw( GameError(gameErrorNS::FATAL_ERROR, 
							"Error creating Direct3D sprite") );
}

//================================================================
//Initialize D3D presentation parameters
//================================================================
void Graphics::initD3Dpp()
{
	try{
		ZeroMemory( &d3dpp, sizeof(d3dpp) );// fills the structure with 0
			//Fill in the parameters we need.
		d3dpp.BackBufferWidth = width;
		d3dpp.BackBufferHeight = height;
		///---------------------------------------------------------------
		if( fullscreen )
			d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8; // 24-bit color
		else
			d3dpp.BackBufferFormat = D3DFMT_UNKNOWN; // use desktop's settings
		///---------------------------------------------------------------
		d3dpp.BackBufferCount = 1;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.hDeviceWindow = hwnd;
		d3dpp.Windowed = (!fullscreen);
		d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
	}
	catch( ... )
	{
		throw( GameError( gameErrorNS::FATAL_ERROR,
				"Error Initializing D3D presentation parameters"));
	}
}

// display the back buffer
HRESULT Graphics::showBackBuffer()
{
	result = E_FAIL;

	// present the backbuffer to the screen
	result = device3d->Present( NULL, NULL, NULL, NULL );

	return result;						
}

// tests for lost device
HRESULT Graphics::getDeviceState()
{
	result = E_FAIL; // default to fail, replace on success
	if( device3d == NULL ) return result;

	result = device3d->TestCooperativeLevel();//---the method//An application can test a device to see if it is lost 
											  //by calling the TestCooperativeLevel method. the return value normally will be "D3D_OK".
											  //If the method fails, the return value will be one of the following:
											  //D3DERR_DEVICELOST or D3DERR_DEVICENOTRESET or D3DERR_DRIVERINTERNALERROR
	return result;
}

// reset the graphics device
HRESULT Graphics::reset()
{
	result = E_FAIL;
	initD3Dpp();		// D3D presentation parameters
	result = device3d->Reset(&d3dpp);  // attempt to reset graphics device
	return result;
}


bool Graphics::isAdapterCompatible()
{
	UINT modes = direct3d->GetAdapterModeCount( D3DADAPTER_DEFAULT,
		d3dpp.BackBufferFormat);

	for( UINT i = 0; i < modes; i++ )
	{
		result = direct3d->EnumAdapterModes( D3DADAPTER_DEFAULT,
											d3dpp.BackBufferFormat,
											i,
											&pMode );
		if( pMode.Height == d3dpp.BackBufferHeight &&
			pMode.Width == d3dpp.BackBufferWidth &&
			pMode.RefreshRate >= d3dpp.FullScreen_RefreshRateInHz )
		{
			return true;
		}
	}
	return false;
}

//=================================================================
//(114; 5.3.3 Loading the Texture)
//=================================================================
HRESULT Graphics::loadTexture( const char* filename, COLOR_ARGB transcolor, 
								UINT &width, UINT &height, LP_TEXTURE &texture )
{
	// struct for read file info
	D3DXIMAGE_INFO info; //D3DXIMAGE_INFO structure  syntax(115)
	result = E_FAIL;

	try {
		if( filename == NULL )  // no filename!
			{
				texture = NULL;
				return D3DERR_INVALIDCALL; 
			}
		// get width & height from file
		result = D3DXGetImageInfoFromFile( filename, &info ); //syntax(113)
		if( result != D3D_OK )
		{
			return result;
		}
		width = info.Width;
		height = info.Height;

		result = D3DXCreateTextureFromFileEx( 
			device3d,				// LPDIRECT3DDEVICE9	3D Device
			filename,				// LPCTSTR				image filename
			info.Width,				// UINT					texture width
			info.Height,			// UINT					texture height
			1,						// UINT					mip-map levels ( 1 for no chain )
			0,						// DWORD				usage
			D3DFMT_UNKNOWN,			// D3DFORMAT			surface format (default)
			D3DPOOL_DEFAULT,		// D3DPOOL				memory class for the texture
			D3DX_DEFAULT,			// DWORD				image filter
			D3DX_DEFAULT,			// DWORD				mip filter
			transcolor,				// D3DCOLOR				color key for transparency
			&info,					// D3DXIMAGE			bitmap file info (from loaded file)
			NULL,					// PALETTEENTRY			color palette
			&texture );				// LPDIRECT3DTEXTURE9	destination texture

		}
	catch(...) 
		{
			throw( GameError( gameErrorNS::FATAL_ERROR, "Error in Graphics::loadTexture" ));
		}
	return result;
}


void Graphics::drawSprite( const SpriteData &spriteData, COLOR_ARGB color )
{
	if( spriteData.texture == NULL ) // if no texture
		return;

	// find the center of sprite ---so we can rotate the sprite around that point.
	VECTOR2 spriteCenter = VECTOR2( (float)(spriteData.width/2*spriteData.scale),
									(float)(spriteData.height/2*spriteData.scale) );

	// screen position of the sprite 
	//We will use the translate vector later to create a transformation matrix
	VECTOR2 translate = VECTOR2( (float)spriteData.x, (float)spriteData.y );

	// scaling in X & Y
	//initialized with the scale value from the spriteData structure.
	//Sprites may be scaled to different sizes in the x and y dimension.We use the same scaling factor for both.
	VECTOR2 scaling( (float)spriteData.scale, (float)spriteData.scale );

	//if flip horizontal
	if( spriteData.flipHorizontal )  // when we wanna flip the sprite, we need to set up a boolean value for flipHorizontal.
	{
		scaling.x *= -1;	// negative X scale to flip
		// get center of flipped image
		spriteCenter.x -= (float)(spriteData.width * spriteData.scale);

		translate.x += (float)(spriteData.width * spriteData.scale);
	}
	if( spriteData.flipVertical )
	{
		scaling.y *= -1;
		spriteCenter.y -= (float)(spriteData.height * spriteData.scale);
		translate.y += (float)(spriteData.height * spriteData.scale);
	}
	// create a matrix to rotate, scale & position our sprite
	D3DXMATRIX matrix;
	D3DXMatrixTransformation2D( //syntax(117)
		&matrix,		  // D3DXMATRIX 				the matrix
		NULL,			  // const D3DXVECTOR2			keep the origin at top left when scaling
		0.0f,			  // FLOAT						no scaling rotation
		&scaling,		  // const D3DXVECTOR2			scale amount
		&spriteCenter,	  // const D3DXVECTOR2			rotation center
		spriteData.angle, // FLOAT						rotation angle
		&translate );	  // const D3DXVECTOR2			X,Y Location

	// tell the sprite about the matrix. Applies the matrix to the sprite.
	sprite->SetTransform( &matrix );

	//Draw the sprite. adds the sprite to the scene //syntax(119)
	sprite->Draw( spriteData.texture, &spriteData.rect, NULL, NULL, color );
}