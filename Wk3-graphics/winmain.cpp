// Programming 2D Games
// Copyright (c) 2011 by:
// Charles Kelly
// Chapter 2 "Hello World" Windows Style v1.0
// winmain.cpp

#define WIN32_LEAN_AND_MEAN   

#include <windows.h>
#include "Fightgame.h"

// Function prototypes
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int); 
bool CreateMainWindow(HWND &, HINSTANCE, int);
LRESULT WINAPI WinProc(HWND, UINT, WPARAM, LPARAM); 

// Global variable
HINSTANCE hinst;

// Graphics pointer
Fightgame *game = NULL;
HWND hwnd = NULL;

//=============================================================================
// Starting point for a Windows application
// Parameters are:
//   hInstance - handle to the current instance of the application
//   hPrevInstance - always NULL, obsolete parameter, maintained for backwards compatibilty
//   lpCmdLine - pointer to null-terminated string of command line arguments
//   nCmdShow - specifies how the window is to be shown
//=============================================================================
int WINAPI WinMain( HINSTANCE hInstance,
                    HINSTANCE hPrevInstance,
                    LPSTR     lpCmdLine,
                    int       nCmdShow)
{
    MSG	 msg;
	
	game = new Fightgame();

    // Create the window
    if (!CreateMainWindow(hwnd, hInstance, nCmdShow))
        return 1;

	try 
	{
		game->initialize(hwnd);
//---------------------------------------------------------		
// main message loop (23; 2.25 Message Loop)---also the game loop
//---------------------------------------------------------
//Windows will communicate with our program by sending it messages.
		int done = 0;		// 1== true;0==false;
		while (!done)		// while(done==false)
		{
										// PeekMessage is a non-blocking method for checking for Windows messages.
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
			{
										//look for quit message
				if (msg.message == WM_QUIT)
					done = 1;

									   //decode and pass messages on to WinProc
									   //If our application is going to accept character input from the user, it needs to
									   //call the TranslateMessage function inside the message loop
				TranslateMessage(&msg);//TranslateMessage is a Windows function that translates virtual - key messages
									   //into character messages
/*to WinProc*/  DispatchMessage(&msg);//The messagesare sent to our WinProc function for processing by the DispatchMessage function.
			}
			else
			{
				game->run(hwnd); // Run the game loop
			}
		}//end while
		SAFE_DELETE( game );
		return msg.wParam;
	}// end try
	catch( const GameError &err )
	{
		game->deleteAll();
		DestroyWindow(hwnd);
		MessageBox( NULL, err.getMessage(), "Error", MB_OK );
	}
	catch( ... )
	{
		game->deleteAll();
		DestroyWindow(hwnd);
		MessageBox( NULL, "Unknown error has occured in game.", "Error", MB_OK );
	}
	SAFE_DELETE( game );
	return 0;
}

//=============================================================================
// window event callback function
//=============================================================================
//-----------------------------------------------------------------------------
//(23; 2.2.6 2.2.6 WinProc Function)
//-----------------------------------------------------------------------------
/*
LRESULT WINAPI WinProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )//The name of this function(WinProc) must match
{																		   //the name specified in the WNDCLASSEX structure																		   
	switch( msg )														   //in the CreateWindowClass function.
	{
	case WM_DESTROY:
		PostQuitMessage(0);// Tell Windows to kill this program
		return 0;
	case WM_CHAR: // A character was entered by the keyboard
		switch (wParam) // The character is in wParam
		{
			case 0x08: // Backspace
			case 0x09: // Tab
			case 0x0A: // Linefeed
			case 0x0D: // Carriage return
			case 0x1B: // Escape
				MessageBeep((UINT) -1); // Beep but do not display
				return 0;
			default: // Displayable character
				ch = (TCHAR) wParam; // Get the character
				InvalidateRect(hwnd, NULL, TRUE); // Force WM_PAINT
				return 0;
	   }
	case WM_PAINT: // The window needs to be redrawn
		hdc = BeginPaint(hwnd, &ps); // Get handle to device context
		GetClientRect(hwnd, &rect); // Get the window rectangle
		// Display the character
		TextOut(hdc, rect.right/2, rect.bottom/2, &ch, 1);
		EndPaint(hwnd, &ps);
		return 0;
	default:
		return DefWindowProc( hWnd, msg, wParam, lParam );
	}

	case WM_KEYDOWN: // Key down
		vkKeys[wParam] = true;
		switch(wParam)
		{
			case VK_SHIFT: // Shift key
				nVirtKey = GetKeyState(VK_LSHIFT);			 // Get state of left shift
			
				if (nVirtKey & SHIFTED)						// If left shift
					vkKeys[VK_LSHIFT] = true;
				nVirtKey = GetKeyState(VK_RSHIFT);			// Get state of right shift
											
				if (nVirtKey & SHIFTED)						// If right shift
					vkKeys[VK_RSHIFT] = true;
				break;

			case VK_CONTROL:								// Control key
				nVirtKey = GetKeyState(VK_LCONTROL);
				if (nVirtKey & SHIFTED)						 // If left control
					vkKeys[VK_LCONTROL] = true;
				nVirtKey = GetKeyState(VK_RCONTROL);
				if (nVirtKey & SHIFTED)						// If right control
					vkKeys[VK_RCONTROL] = true;
					break;
			}
			InvalidateRect(hwnd, NULL, TRUE); // Force WM_PAINT
			return 0;
			break;
		case WM_KEYUP: // Key up
			vkKeys[wParam] = false;
			switch(wParam)
			{
				case VK_SHIFT: // Shift key
					nVirtKey = GetKeyState(VK_LSHIFT);
					if ((nVirtKey & SHIFTED) == 0) // If left shift
						vkKeys[VK_LSHIFT] = false;
					nVirtKey = GetKeyState(VK_RSHIFT);
					if ((nVirtKey & SHIFTED) == 0) // If right shift
						vkKeys[VK_RSHIFT] = false;
					break;
				case VK_CONTROL: // Control key
					nVirtKey = GetKeyState(VK_LCONTROL);
					if ((nVirtKey & SHIFTED) == 0) // If left control
						vkKeys[VK_LCONTROL] = false;
					nVirtKey = GetKeyState(VK_RCONTROL);
					if ((nVirtKey & SHIFTED) == 0) // If right control
						vkKeys[VK_RCONTROL] = false;
					break;
			}
			InvalidateRect(hwnd, NULL, TRUE); // Force WM_PAINT
			return 0;
			break;
}
*/
//-----------------------------------------------------------------------------
LRESULT WINAPI WinProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	return( game->messageHandler( hWnd, msg, wParam, lParam) );
}

//=============================================================================
// Create the window
// Returns: false on error
//=============================================================================
bool CreateMainWindow( HWND &hwnd, HINSTANCE hInstance, int nCmdShow) 
{
//--------------------------------------------------------------------
// Creat a window class
//--------------------------------------------------------------------
    WNDCLASSEX wcx; //(18; 2.2.3 Windos Class) structure
 
    // Fill in the window class structure with parameters 
    // that describe the main window. 
	// (19; List 2.3)
    wcx.cbSize = sizeof(wcx);           // size of structure 
    wcx.style = CS_HREDRAW | CS_VREDRAW;    // redraw if size changes 
    wcx.lpfnWndProc = WinProc;          // points to window procedure 
    wcx.cbClsExtra = 0;                 // no extra class memory 
    wcx.cbWndExtra = 0;                 // no extra window memory 
    wcx.hInstance = hInstance;          // handle to instance 
    wcx.hIcon = NULL; 
    wcx.hCursor = LoadCursor(NULL,IDC_ARROW);   // predefined arrow 
    wcx.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);    // black background brush 
    wcx.lpszMenuName =  NULL;           // name of menu resource 
    wcx.lpszClassName = CLASS_NAME;     // name of window class 
    wcx.hIconSm = NULL;                 // small class icon 
 
    // Register the window class. 
    // RegisterClassEx returns 0 on error. 
	//(20; )
    if (RegisterClassEx(&wcx) == 0)    // if error
        return false;

	//set up the screen in windowed for fullscreen mode
	DWORD style;
	if( FULLSCREEN )
		style = WS_EX_TOPMOST | WS_VISIBLE | WS_POPUP;
	else
		style = WS_OVERLAPPEDWINDOW;//overlapped window
//-----------------------------------------------------------------------
//creat an actual window (20; 2.2.4 CreateWindow Function)
//-----------------------------------------------------------------------
/*		HWND CreateWindow(
		LPCTSTR lpClassName,
		LPCTSTR lpWindowName,
		DWORD dsStyle,
		int x,
		int y,
		int nWidth,
		int nHeight,
		HWND hWndParent,
		HMENU hMenu,
		HINSTANCE hInstance,
		LPVOID lpParam
		);
*/
//-----------------------------------------------------------------------
    // Create window
    hwnd = CreateWindow(
        CLASS_NAME,             // name of the window class
        GAME_TITLE,              // title bar text
        style,					// window style
        CW_USEDEFAULT,          // default horizontal position of window :0
        CW_USEDEFAULT,          // default vertical position of window	:0
        GAME_WIDTH,           // width of window
        GAME_HEIGHT,          // height of the window
        (HWND) NULL,            // no parent window
        (HMENU) NULL,           // no menu
        hInstance,              // handle to application instance. The application identifier from the Window class.
        (LPVOID) NULL);         // no window parameters

    // if there was an error creating the window
    if (!hwnd)
        return false;

	if( !FULLSCREEN )
	{
		// adjust window size so client area is GAME_WIDTH x GAME_HEIGHT
		RECT clientRect;//The RECT structure defines the coordinates of the upper-left and lower-right corners of a rectangle.
		GetClientRect( hwnd, &clientRect );
		MoveWindow( hwnd,
					0,	// left 
					0,  // Top
					GAME_WIDTH+(GAME_WIDTH-clientRect.right), // Right
					GAME_HEIGHT+(GAME_HEIGHT-clientRect.bottom), // bottom
					true );
	}

    // Show the window
    ShowWindow(hwnd, nCmdShow);

    // Send a WM_PAINT message to the window procedure
    UpdateWindow(hwnd);
    return true;
}
