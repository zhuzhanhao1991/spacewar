#include "Warrior.h"

Warrior::Warrior() : Entity()
{
	spriteData.width = warriorNS::WIDTH;
	spriteData.height = warriorNS::HEIGHT;
	spriteData.x = warriorNS::X;
	spriteData.y = warriorNS::Y;
	spriteData.rect.right = warriorNS::WIDTH;
	spriteData.rect.bottom = warriorNS::HEIGHT;

	frameDelay = warriorNS::WARRIOR_ANIMATION_DELAY;
	startFrame = warriorNS::WARRIOR1_START_FRAME;
	endFrame = warriorNS::WARRIOR1_END_FRAME;
	currentFrame = startFrame;

	collisionType = entityNS::CIRCLE;
	radius = warriorNS::WIDTH / 2.0f;

	velocity.x = 1;
	velocity.y = 1;
}
//============================================================================================
/*   Warrior::update
If we wanna Warrior has its own update(automaticly update, there is no interaction with out side and other object  )

spriteData.angle += frameTime * warriorNS::ROTATION_RATE;
	spriteData.x += frameTime * velocity.x;
	spriteData.y += frameTime * velocity.y;                                            */
//=============================================================================================
void Warrior::update(float frameTime)
{
	Entity::update(frameTime);

	

	if (spriteData.x > (GAME_WIDTH - warriorNS::WIDTH * getScale()))
	{
		spriteData.x = GAME_WIDTH - warriorNS::WIDTH * getScale();
	}
	else if (spriteData.x < 0)
	{
		spriteData.x = 0;

	}
	if (spriteData.y >(GAME_HEIGHT - warriorNS::HEIGHT * getScale()))
	{
		spriteData.y = GAME_HEIGHT - warriorNS::WIDTH * getScale();
		/*velocity.y = 0;*/
	}
	else if (spriteData.y < 0)
	{
		spriteData.y = 0;
		/*velocity.y = 0;*/
	}
	//Animating the shield.
	//if (shieldOn)
	//{
	//	shield.update(frameTime);
	//	if (shield.getAnimationComplete())
	//	{
	//		shieldOn = false;
	//		shield.setAnimationComplete(false);
	//	}
	//}
}
//========================================================================
// Initialize the ship
// Post: returns true if successful, false if failed
//========================================================================
bool Warrior::initialize(Game *gamePtr, int width, int height, int ncols,
	TextureManager *textureM)
{
	//shield.initialize(gamePtr->getGraphics(), width, height, ncols,
	//	textureM);
	//shield.setFrames(warriorNS::SHIELD_START_FRAME, warriorNS::SHIELD_END_FRAME);
	//shield.setCurrentFrame(warriorNS::SHIELD_START_FRAME);
	//shield.setFrameDelay(warriorNS::SHIELD_ANIMATION_DELAY);
	//shield.setLoop(false); // Do not loop animation
	return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}
//========================================================================
// Draw the ship
//========================================================================
void Warrior::draw()
{
	Image::draw(); // Draw ship
	//if (shieldOn)
	//	// Draw shield using colorFilter 50% alpha
	//	shield.draw(spriteData, graphicsNS::ALPHA50 & colorFilter);
	////ship's spriteData--- shiled will have the same size and rotation of the ship
}



//void Warrior::damage(WEAPON weapon)
//{
//	shieldOn = true;
//}