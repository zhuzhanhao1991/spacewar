#include "bullet.h"

Bullet::Bullet() :Entity()
{
	spriteData.width = BulletNS::WIDTH;
	spriteData.height = BulletNS::HEIGHT;
	spriteData.x = BulletNS::X;
	spriteData.y = BulletNS::Y;
	spriteData.rect.right = BulletNS::WIDTH;
	spriteData.rect.bottom = BulletNS::HEIGHT;

	frameDelay = BulletNS::BULLET_ANIMATION_DELAY;
	startFrame = BulletNS::BULLET_START_FRAME;
	endFrame = BulletNS::BULLET_END_FRAME;
	currentFrame = startFrame;

	collisionType = entityNS::CIRCLE;
	radius = BulletNS::WIDTH / 2.0f;

	velocity.x = 0;
	velocity.y = 0;
}

void Bullet::update(float frameTime)
{
	Entity::update(frameTime);



	if (spriteData.x > (GAME_WIDTH - BulletNS::WIDTH * getScale()))
	{
		spriteData.x = GAME_WIDTH - BulletNS::WIDTH * getScale();
		velocity.x = -velocity.x;
	}
	else if (spriteData.x < 0)
	{
		spriteData.x = 0;
		velocity.x = -velocity.x;

	}
	if (spriteData.y >(GAME_HEIGHT - BulletNS::HEIGHT * getScale()))
	{
		spriteData.y = GAME_HEIGHT - BulletNS::WIDTH * getScale();
		velocity.y = -velocity.y;
	}
	else if (spriteData.y < 0)
	{
		spriteData.y = 0;
		velocity.y = -velocity.y;
	}

}
bool Bullet::initialize(Game *gamePtr, int width, int height, int ncols,
	TextureManager *textureM)
{

	return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

void Bullet::draw()
{
	Image::draw(); 
}