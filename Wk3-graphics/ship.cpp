#include "Ship.h"

Ship::Ship() : Entity()
{
	spriteData.width = shipNS::WIDTH;
	spriteData.height = shipNS::HEIGHT;
	spriteData.x = shipNS::X;
	spriteData.y = shipNS::Y;
	spriteData.rect.right = shipNS::WIDTH;
	spriteData.rect.bottom = shipNS::HEIGHT;

	frameDelay = shipNS::SHIP_ANIMATION_DELAY;
	startFrame = shipNS::SHIP1_START_FRAME;
	endFrame = shipNS::SHIP1_END_FRAME;
	currentFrame = startFrame;

	collisionType = entityNS::CIRCLE;
	radius = shipNS::WIDTH/2.0f;

	velocity.x = 0;
	velocity.y = 0;
}

void Ship::update ( float frameTime )
{
	Entity::update( frameTime );

	//spriteData.angle += frameTime * shipNS::ROTATION_RATE;
	//spriteData.x += frameTime * velocity.x;
	//spriteData.y += frameTime * velocity.y;

	if( spriteData.x > (GAME_WIDTH - shipNS::WIDTH * getScale() ))
	{
		spriteData.x = GAME_WIDTH - shipNS::WIDTH * getScale();
		velocity.x = -velocity.x;
	}
	else if( spriteData.x < 0 )
	{
		spriteData.x = 0;
		velocity.x = -velocity.x; //reverse the direction.
	}
	if( spriteData.y > (GAME_HEIGHT - shipNS::HEIGHT * getScale() ))
	{
		spriteData.y = GAME_HEIGHT - shipNS::WIDTH * getScale();
		velocity.y = -velocity.y;
	}
	else if( spriteData.y < 0 )
	{
		spriteData.y = 0;
		velocity.y = -velocity.y;
	}
	//Animating the shield.
	if (shieldOn)
	{
		shield.update(frameTime);
		if (shield.getAnimationComplete())
		{
			shieldOn = false;
			shield.setAnimationComplete(false);
		}
	}
}
//========================================================================
// Initialize the ship
// Post: returns true if successful, false if failed
//========================================================================
bool Ship::initialize(Game *gamePtr, int width, int height, int ncols,
	TextureManager *textureM)
{
	shield.initialize(gamePtr->getGraphics(), width, height, ncols,
		textureM);
	shield.setFrames(shipNS::SHIELD_START_FRAME, shipNS::SHIELD_END_FRAME);
	shield.setCurrentFrame(shipNS::SHIELD_START_FRAME);
	shield.setFrameDelay(shipNS::SHIELD_ANIMATION_DELAY);
	shield.setLoop(false); // Do not loop animation
	return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}
//========================================================================
// Draw the ship
//========================================================================
void Ship::draw()
{
	Image::draw(); // Draw ship
	if (shieldOn)
		// Draw shield using colorFilter 50% alpha
		shield.draw(spriteData, graphicsNS::ALPHA50 & colorFilter);
		//ship's spriteData--- shiled will have the same size and rotation of the ship
}

void Ship::damage(WEAPON weapon)
{
	shieldOn = true;
}